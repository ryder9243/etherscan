﻿using etherscan.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace etherscan
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                LoadTokenList();
            }
        }

        public void LoadTokenList()
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;

            using (MySqlConnection con = new MySqlConnection(constr))
            {
                using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM token"))
                {
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            GridView1.DataSource = dt;
                            GridView1.DataBind();
                        }
                    }
                }
            }
        }

        [WebMethod]
        public static void SaveToken(Token token)
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;

            using (MySqlConnection con = new MySqlConnection(constr))
            {
                using (MySqlCommand cmd = new MySqlCommand("INSERT INTO token VALUES(@id, @symbol, @name, @total_supply, @contract_address, " +
                    "@total_holders, @price)"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", DBNull.Value);
                    cmd.Parameters.AddWithValue("@symbol", token.symbol);
                    cmd.Parameters.AddWithValue("@name", token.name);
                    cmd.Parameters.AddWithValue("@total_supply", token.total_supply);
                    cmd.Parameters.AddWithValue("@contract_address", token.contract_address);
                    cmd.Parameters.AddWithValue("@total_holders", token.total_holders);
                    cmd.Parameters.AddWithValue("@price", 0.00);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }

        [WebMethod]
        public static void UpdateToken(Token token)
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;

            using (MySqlConnection con = new MySqlConnection(constr))
            {
                using (MySqlCommand cmd = new MySqlCommand("UPDATE token Set symbol=@symbol, name=@name, total_supply=@totalsupply," +
                    "contract_address=@contractaddress, total_holders=@totalholders WHERE id = @id"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", token.id);
                    cmd.Parameters.AddWithValue("@symbol", token.symbol);
                    cmd.Parameters.AddWithValue("@name", token.name);
                    cmd.Parameters.AddWithValue("@totalsupply", token.total_supply);
                    cmd.Parameters.AddWithValue("@contractaddress", token.contract_address);
                    cmd.Parameters.AddWithValue("@totalholders", token.total_holders);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }

        [WebMethod]
        public static List<PieChart> GetPieChart()
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;

            List<PieChart> pieCharts = new List<PieChart>();

            using (MySqlConnection con = new MySqlConnection(constr))
            {
                using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM token", con))
                {
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            PieChart pie = new PieChart();
                            pie.name = sdr["name"].ToString();
                            pie.value = Convert.ToInt32(sdr["total_supply"]);
                            pieCharts.Add(pie);
                        }
                    }
                    con.Close();
                }
            }

            return pieCharts;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblTotalSupply = (e.Row.FindControl("lblTotalSupply") as Label);
                Label lblTotalSupplyPercentage = (e.Row.FindControl("lblTotalSupplyPercentage") as Label);

                double totalSupply = GetTokenTotalSupply();

                double percentageSupply = (Convert.ToDouble(lblTotalSupply.Text) / totalSupply) * 100;
                lblTotalSupplyPercentage.Text = percentageSupply.ToString();
            }
        }

        private double GetTokenTotalSupply()
        {
            double val = 0;

            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;

            using (MySqlConnection con = new MySqlConnection(constr))
            {
                using (MySqlCommand cmd = new MySqlCommand("SELECT SUM(total_supply) as totalSupply FROM token", con))
                {
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.Read())
                        {
                            val = Convert.ToDouble(sdr["totalSupply"]);
                        }
                    }
                    con.Close();
                }
            }

            return val;
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            /*Fill Gridview after page change*/
            GridView1.PageIndex = e.NewPageIndex;
            this.LoadTokenList();
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "SelectSymbol")
            {
                Response.Redirect(string.Format("~/Detail.aspx?id={0}", e.CommandArgument));
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=TokenList.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            GridView1.AllowPaging = false;
            LoadTokenList();
            StringBuilder Sb = new StringBuilder();
            for (int k = 0; k < GridView1.Columns.Count; k++)
            {
                if (!string.IsNullOrEmpty(GridView1.Columns[k].HeaderText))
                {
                    Sb.Append(GridView1.Columns[k].HeaderText + ',');
                }
            }
            Sb.Append("\r\n");
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if ((GridView1.Rows[i].FindControl("lblId") as Label) != null)
                {
                    Sb.Append((GridView1.Rows[i].FindControl("lblId") as Label).Text + ',');
                }

                if ((GridView1.Rows[i].FindControl("lblSymbol") as LinkButton) != null)
                {
                    Sb.Append((GridView1.Rows[i].FindControl("lblSymbol") as LinkButton).Text + ',');
                }

                if ((GridView1.Rows[i].FindControl("lblName") as Label) != null)
                {
                    Sb.Append((GridView1.Rows[i].FindControl("lblName") as Label).Text + ',');
                }

                if ((GridView1.Rows[i].FindControl("lblContractAddress") as Label) != null)
                {
                    Sb.Append((GridView1.Rows[i].FindControl("lblContractAddress") as Label).Text + ',');
                }

                if ((GridView1.Rows[i].FindControl("lblTotalHolders") as Label) != null)
                {
                    Sb.Append((GridView1.Rows[i].FindControl("lblTotalHolders") as Label).Text + ',');
                }

                if ((GridView1.Rows[i].FindControl("lblTotalSupply") as Label) != null)
                {
                    Sb.Append((GridView1.Rows[i].FindControl("lblTotalSupply") as Label).Text + ',');
                }

                if ((GridView1.Rows[i].FindControl("lblTotalSupplyPercentage") as Label) != null)
                {
                    Sb.Append((GridView1.Rows[i].FindControl("lblTotalSupplyPercentage") as Label).Text + ',');
                }
                Sb.Append("\r\n");
            }
            Response.Output.Write(Sb.ToString());
            Response.Flush();
            Response.End();
        }
    }
}
﻿<%@ Page Title="Detail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Detail.aspx.cs" Inherits="etherscan.Detail" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        $(document).ready(function () {

            $('#btnConfirm').click(function () {

                var id = GetParameterValues('id');

                $.ajax({
                    type: "POST",
                    url: "Detail.aspx/DeleteToken",
                    data: '{"tokenId": "' + id + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        window.location.href = "Default.aspx";
                    }
                });

                function GetParameterValues(param) {
                    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                    for (var i = 0; i < url.length; i++) {
                        var urlparam = url[i].split('=');
                        if (urlparam[0] == param) {
                            return urlparam[1];
                        }
                    }
                }

                return false;
            });
        });
    </script>

    <div class="row">
        <div class="col-md-12 mt-5">

            <div class="card">
                <div class="card-header">
                    <p id="contractAddress" runat="server"></p>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    Price: <span id="txtPrice" runat="server"></span>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    Total Supply: <span id="txtTotalSupply" runat="server"></span>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    Total Holders: <span id="txtTotalHolders" runat="server"></span>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    Name: <span id="txtName" runat="server"></span>
                </div>
            </div>

            <asp:Button ID="btnBack" type="button" class="btn btn-primary mt-3" runat="server" OnClick="btnBack_Click" Text="Back"></asp:Button>
            <button type="button" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger mt-3" runat="server">Delete</button>
        </div>

        <!-- Modal for Deletion-->
        <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Warning</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="btnConfirm" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

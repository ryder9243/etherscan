﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="etherscan._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">

        var selectedGridviewRow;

        $(document).ready(function () {

            //========================== Validate form ==========================
            $('#namecheck').hide();
            let nameError = true;
            $('#txtName').keyup(function () {
                validateName();
            });

            function validateName() {
                let nameValue = $('#txtName').val();
                if (nameValue.length == '') {
                    $('#namecheck').show();
                    nameError = false;
                }
                else {
                    $('#namecheck').hide();
                    nameError = true;
                }
            }

            $('#symbolcheck').hide();
            let symbolError = true;
            $('#txtSymbol').keyup(function () {
                validateSymbol();
            });

            function validateSymbol() {
                let symbolValue = $('#txtSymbol').val();
                if (symbolValue.length == '') {
                    $('#symbolcheck').show();
                    symbolError = false;
                }
                else if (symbolValue.length > 5) {
                    $('#symbolcheck').show();
                    $('#symbolcheck').html
                        ("**length of symbol must be between less than 5");
                }
                else {
                    $('#symbolcheck').hide();
                    symbolError = true;
                }
            }

            $('#contractaddresscheck').hide();
            let addressError = true;
            $('#txtAddress').keyup(function () {
                validateAddress();
            });

            function validateAddress() {
                let addressValue = $('#txtAddress').val();
                if (addressValue.length == '') {
                    $('#contractaddresscheck').show();
                    addressError = false;
                }
                else {
                    $('#contractaddresscheck').hide();
                    addressError = true;
                }
            }

            $('#totalsupplycheck').hide();
            let supplyError = true;
            $('#txtTotalSupply').keyup(function () {
                validateSupply();
            });

            function validateSupply() {
                let totalsupplyValue = $('#txtTotalSupply').val();
                if (totalsupplyValue.length == '') {
                    $('#totalsupplycheck').show();
                    supplyError = false;
                }
                else {
                    $('#totalsupplycheck').hide();
                    supplyError = true;
                }
            }

            $('#totalholderscheck').hide();
            let holderError = true;
            $('#txtTotalHolders').keyup(function () {
                validateHolders();
            });

            function validateHolders() {
                let holderValue = $('#txtTotalHolders').val();
                if (holderValue.length == '') {
                    $('#totalholderscheck').show();
                    holderError = false;
                }
                else {
                    $('#totalholderscheck').hide();
                    holderError = true;
                }
            }
            //========================== Validate form ==========================

            //========================== Save/Update form ==========================
            $('#btnSave').click(function () {
                //Validate each input
                validateName();
                validateSymbol();
                validateAddress();
                validateSupply();
                validateHolders();

                if ((nameError == true) &&
                    (symbolError == true) &&
                    (addressError == true) &&
                    (supplyError == true) &&
                    (holderError == true)) {
                    var token = {};
                    token.id = selectedGridviewRow;
                    token.name = $("[id*=txtName]").val();
                    token.symbol = $("[id*=txtSymbol]").val();
                    token.contract_address = $("[id*=txtAddress]").val();
                    token.total_supply = $("[id*=txtTotalSupply]").val();
                    token.total_holders = $("[id*=txtTotalHolders]").val();

                    var btnText = $("#btnSave").text();

                    if (btnText == 'Save') {
                        $.ajax({
                            type: "POST",
                            url: "Default.aspx/SaveToken",
                            data: '{token: ' + JSON.stringify(token) + '}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                window.location.reload();
                            }
                        });
                    } else {
                        $.ajax({
                            type: "POST",
                            url: "Default.aspx/UpdateToken",
                            data: '{token: ' + JSON.stringify(token) + '}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                window.location.reload();
                            }
                        });
                    }
                }

                return false;
            });
            //========================== Save/Update form ==========================

            //========================== Reset/Clear form ==========================
            $('#btnReset').click(function () {
                $('#txtName').val("");
                $('#txtSymbol').val("");
                $('#txtAddress').val("");
                $('#txtTotalSupply').val("");
                $('#txtTotalHolders').val("");
                $('#btnSave').text('Save');
            });
            //========================== Reset/Clear form ==========================

            //========================== Display Pie Chart ==========================
            $(function () {
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/GetPieChart",
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: DrawPieChart,
                    failure: function (response) {
                        alert(response.d);
                    },
                    error: function (response) {
                        alert(response.d);
                    }
                });
            });

            function DrawPieChart(response) {
                var aData = response.d;
                var arr = []

                $.map(aData, function (item, index) {
                    var i = [item.name, item.value];
                    var obj = {};
                    obj.name = item.name;
                    obj.y = item.value;
                    arr.push(obj);
                });

                var jsonArray = JSON.parse(JSON.stringify(arr));

                Highcharts.chart('container', {
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: 'Token Statistics by Total Supply'
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: false
                    },

                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                            }
                        }
                    },

                    series: [{
                        name: 'Total Supply',
                        colorByPoint: true,
                        data: jsonArray
                    }],
                })
            };
            //========================== Display Pie Chart ==========================
            
            //========================== Gridview Link Edit ==========================
            $(function () {
                $("[id*=GridView1]").find("[id*=lnkEdit]").click(function () {
                    //Reference the GridView Row.
                    var row = $(this).closest("tr");
                    var selectedRow = (row[0].rowIndex - 1);

                    $("#txtName").val($("#MainContent_GridView1_lblName_" + selectedRow).html());
                    $("#txtSymbol").val($("#MainContent_GridView1_lblSymbol_" + selectedRow).html());
                    $("#txtAddress").val($("#MainContent_GridView1_lblContractAddress_" + selectedRow).html());
                    $("#txtTotalSupply").val($("#MainContent_GridView1_lblTotalSupply_" + selectedRow).html());
                    $("#txtTotalHolders").val($("#MainContent_GridView1_lblTotalHolders_" + selectedRow).html());
                    selectedGridviewRow = $("#MainContent_GridView1_lblId_" + selectedRow).html();

                    var btnText = $("#btnSave").text();

                    if (btnText == 'Save') {
                        $("#btnSave").text('Update');
                    }

                    return false;
                });
            });
            //========================== Gridview Link Edit ==========================
        });
    </script>

    <div class="row mt-5">
        <div class="col-md-6">
            <h5>Save / Update Token</h5>
            <div class="form-group row">
                <label for="txtName" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="txtName" placeholder="Name">
                </div>
                <h5 id="namecheck" style="color: red;">**name is missing
                </h5>
            </div>

            <div class="form-group row">
                <label for="txtSymbol" class="col-sm-2 col-form-label">Symbol</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="txtSymbol" placeholder="Symbol">
                </div>
                <h5 id="symbolcheck" style="color: red;">**symbol is missing
                </h5>
            </div>

            <div class="form-group row">
                <label for="txtAddress" class="col-sm-2 col-form-label">Contract Address</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="txtAddress" placeholder="Contract Address">
                </div>
                <h5 id="contractaddresscheck" style="color: red;">**contract address is missing
                </h5>
            </div>

            <div class="form-group row">
                <label for="txtTotalSupply" class="col-sm-2 col-form-label">Total Supply</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="txtTotalSupply" placeholder="Total Supply">
                </div>
                 <h5 id="totalsupplycheck" style="color: red;">**total supply is missing
                </h5>
            </div>

            <div class="form-group row">
                <label for="txtTotalHolders" class="col-sm-2 col-form-label">Total Holders</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="txtTotalHolders" placeholder="Total Holders">
                </div>
                 <h5 id="totalholderscheck" style="color: red;">**total holders is missing
                </h5>
            </div>

            <div class="form-group row">
                <div class="container">
                    <button type="button" id="btnSave" class="btn btn-primary">Save</button>
                    <button type="button" id="btnReset" class="btn btn-info">Reset</button>
                </div>
            </div>
        </div>

        <%-- Pie Chart Container--%>
        <div class="col-md-6" id="container">
        </div>

        <div class="col-md-12">
            <asp:GridView ID="GridView1" class="table table-bordered"
                runat="server" AutoGenerateColumns="false" OnRowDataBound="GridView1_RowDataBound"
                OnPageIndexChanging="GridView1_PageIndexChanging" AllowPaging="true" PageSize="10"
                OnRowCommand="GridView1_RowCommand">
                <Columns>
                    <asp:TemplateField HeaderText="Rank">
                        <ItemTemplate>
                            <asp:Label ID="lblId" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Symbol">
                        <ItemTemplate>
                            <asp:LinkButton ID="lblSymbol" CommandName="SelectSymbol" CommandArgument='<%# Eval("id") %>' runat="server" Text='<%# Eval("symbol") %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Contract Address">
                        <ItemTemplate>
                            <asp:Label ID="lblContractAddress" runat="server" Text='<%# Eval("contract_address") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total Holders">
                        <ItemTemplate>
                            <asp:Label ID="lblTotalHolders" runat="server" Text='<%# Eval("total_holders") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total Supply">
                        <ItemTemplate>
                            <asp:Label ID="lblTotalSupply" runat="server" Text='<%# Eval("total_supply") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total Supply %">
                        <ItemTemplate>
                            <asp:Label ID="lblTotalSupplyPercentage" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEdit" Text="Edit" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            <asp:Button ID="btnExport" CssClass="btn btn-success" runat="server" Text="Export to CSV" OnClick="btnExport_Click" />
        </div>
    </div>

</asp:Content>

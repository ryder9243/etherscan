﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace etherscan.Model
{
    public class PieChart
    {
        public string name { get; set; }
        public double value { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace etherscan.Model
{
    public class Token
    {
        public string id { get; set; }
        public string symbol { get; set; }
        public string name { get; set; }
        public string total_supply { get; set; }
        public string contract_address { get; set; }
        public string total_holders { get; set; }
    }
}
﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Web.Services;
using System.Web.UI;

namespace etherscan
{
    public partial class Detail : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Check if Querystring is null or empty
                if (Request.QueryString["id"] != null && Request.QueryString["id"] != string.Empty)
                    LoadTokenDetails(Request.QueryString["id"]);
                else
                    Response.Redirect("Default.aspx");
            }
        }

        private void LoadTokenDetails(string id)
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;

            using (MySqlConnection con = new MySqlConnection(constr))
            {
                using (MySqlCommand cmd = new MySqlCommand("SELECT contract_address, price," +
                    "total_supply, total_holders, name, symbol FROM Token Where id=@id", con))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    con.Open();

                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.Read())
                        {
                            contractAddress.InnerText = sdr["contract_address"].ToString();
                            txtPrice.InnerText = "$ " + sdr["price"].ToString();
                            txtTotalSupply.InnerText = sdr["total_supply"].ToString() + " " + sdr["symbol"].ToString();
                            txtTotalHolders.InnerText = sdr["total_holders"].ToString();
                            txtName.InnerText = sdr["name"].ToString();
                        }
                    }
                    con.Close();
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Detail.aspx");
        }

        [WebMethod]
        public static void DeleteToken(string tokenId)
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;

            using (MySqlConnection con = new MySqlConnection(constr))
            {
                using (MySqlCommand cmd = new MySqlCommand("DELETE FROM token WHERE id=@id"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", tokenId);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
    }
}